#!/usr/bin/env python3
'''
Dolar - Ejemplo de uso del módulo cotiz

Asignatura: Paradigmas de la Programación, LCIk (FP-UNA)
Autor: Prof. Carlos Zayas (czayas en gmail)
Fecha: 07/11/2019 (creación)
'''

from cotiz import *
from datetime import datetime

# Obtener la fecha y hora del sistema:
fechahora = datetime.now()
fecha = datetime.strftime(fechahora, "%d/%m/%Y")

# Obtener la información del web service:
datos = obtener(MELIZECHE)

# Sólo nos interesa el primer diccionario:
dolarpy = datos['dolarpy']

# Crear una lista de entidades financieras:
entidades = [clave for clave in dolarpy]

# Crear una lista de listas con la entidad y su cotización del dólar a la venta:
venta = [[clave, dolarpy[clave]['venta']] for clave in entidades]

# Dos maneras de crear un diccionario en base a la lista venta:
venta_dic1 = dict(venta)
venta_dic3 = {clave: valor for clave, valor in venta}

# Dos maneras de crear un diccionario si no se tiene la lista venta:
venta_dic2 = dict([[clave, dolarpy[clave]['venta']] for clave in entidades])
venta_dic4 = {clave: dolarpy[clave]['venta'] for clave in entidades}

#-------------------------------------------------------------------------------
# Crear una página web (estilo convencional):
#-------------------------------------------------------------------------------

archivo = open("cotiz1.html","w")

archivo.write("<h1>Cotización del dolar al {}</h1>".format(fecha))

archivo.write("<table border=1>\n")
archivo.write("<tr><th>Entidad</th><th>Venta</th></tr>\n")

for fila in venta:
    archivo.write(
        "<tr><td>{0}</td><td>{1}</td></tr>\n".format(fila[0], fila[1]))

archivo.write("</table>\n")
archivo.close()

#-------------------------------------------------------------------------------
# Crear una página web (con with y writelines):
#-------------------------------------------------------------------------------

with open("cotiz2.html", "w") as archivo:

    archivo.write("<h1>Cotización del dolar al {}</h1>".format(fecha))

    archivo.write("<table border=1>\n")
    archivo.write("<tr><th>Entidad</th><th>Venta</th></tr>\n")

    archivo.writelines(
        "<tr><td>{}</td><td>{}</td></tr>\n".format(*fila) for fila in venta)

    archivo.write("</table>\n")

#-------------------------------------------------------------------------------
# Crear una página web (con with, writelines y un diccionario):
#-------------------------------------------------------------------------------

with open("cotiz3.html", "w") as archivo:

    archivo.write("<h1>Cotización del dolar al {}</h1>".format(fecha))

    archivo.write("<table border=1>\n")
    archivo.write("<tr><th>Entidad</th><th>Venta</th></tr>\n")

    archivo.writelines(
        "<tr><td>{}</td><td>{}</td></tr>\n".format(*items)
            for items in venta_dic1.items())

    archivo.write("</table>\n")

#-------------------------------------------------------------------------------
# Crear un archivo INI:
#-------------------------------------------------------------------------------

with open("cotiz.ini", "w") as archivo:

    archivo.write("[info]\n")
    archivo.write("fecha={}\n".format(fecha))
    archivo.write("url={}\n\n".format(MELIZECHE))

    archivo.write("[dolar]\n")
    archivo.writelines("{}={}\n".format(*fila) for fila in venta)

#-------------------------------------------------------------------------------
# Crear un archivo LOG:
#-------------------------------------------------------------------------------

with open("cotiz.log", "a") as archivo:
    archivo.write(datetime.strftime(fechahora, "%Y-%m-%d %H:%M:%S") + "\n")
